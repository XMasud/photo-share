<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CustomController extends Controller
{

    /*This controller was to created to verify login,in this case it was custom login*/
    public function loginCheck(Request $request){

        $email = $request->input('email');
        $pass = $request->input('password');

        if(Auth::attempt(['email' => $email, 'password' => $pass])){


            return redirect()->intended('publisher/home');

        }else{
            Session::flash('message', 'Please check your credentials or contact your administrator!');
            return redirect('login');

        }
    }
    /*Logout*/
    public function logout(){

        Auth::logout();
        Session::flush();
        return redirect('/login');
    }
}
