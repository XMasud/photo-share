<?php

namespace App\Http\Controllers;

use App\Album;
use App\Comments;
use App\Photo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Session;

class PublisherController extends Controller
{
    //
    public function viewHome(){

        /*Home page view for individual publisher it will return with each individual users albums*/
        $userId = Auth::user()->id;
        $album = Album::where('user_id','=',$userId)->get();

       // return $album;
        return view('publisher',['albums'=> $album]);
    }

    public function addPublisher(Request $request)
    {
        /*It will create publisher*/
        $user = new User();

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->password = bcrypt($request->input('password'));
        $user->role = 'Publisher';
        $user->api_token = str_random(60);

        if ($user->save()) {

            Session::flash('message', 'Publisher Created Successfully!');

        };
        return redirect('/login');
    }

    public function albumView(){

        /*It will return add album page*/

        $userId = Auth::user()->id;
        $album = Album::where('user_id','=',$userId)->paginate(8);

        return view('addAlbum',['albums' => $album]);
    }

    public function saveAlbum(Request $request){

        /*Create Album for Publisher*/

        $addAlbum = new Album();

        $addAlbum->name = $request->name;
        $addAlbum->user_id = Auth::user()->id;
        $addAlbum->token = str_random(30);
        $addAlbum->save();
        return response()->json($addAlbum);

    }

    public function editAlbum(Request $request){
        /*Edit Album*/

        $editAlbum = Album::find($request->id);
        $editAlbum->name = $request->name;
        $editAlbum->update();

        return response()->json($editAlbum);
    }

    public function goAlbum(Request $request,$id){

        /*Go individual album with publisher info*/

        $album = Album::where('id','=',$id)->with('user')->first();
        return view('albumPhoto',['album'=>$album]);
    }

    public function addPhoto(Request $request){
        //return $request->all();

        /*It will save multiple photo in Photo Model */
        $destinationPath = 'images/photos'; // upload path

        for($i=0; $i < count($request->images); $i++)
        {
            $photo = new Photo();

            $extension = ($request->file('images')[$i]->getClientOriginalExtension()); // getting image extension
            $fileName = time() . rand(10000, 99999) . '.' . $extension; // renameing image

            $photo->name= $fileName;
            $photo->album_id= $request->input('album_id');
            $photo->save()[$i];
            $request->file('images')[$i]->move($destinationPath, $fileName);
        }

        return redirect('add/album');

    }
    public function viewPhoto(Request $request, $id){

        /*View photos in album and using encryption on album id so in url section user can not change album id */

        $id =  Crypt::decryptString($id);

        $comment = Comments::where('album_id','=',$id)->get();

        $photo = Photo::where('album_id','=',$id)->get();

        return view('viewPhoto',['photos'=> $photo,'comments' =>$comment,'id'=>$id]);

    }

    public function albumPasswordSet(Request $request){

        /*Set password for each album*/

        $album = Album::find($request->id);

        $album->password = bcrypt($request->input('password'));
        $album->update();
        return response()->json($album);

    }

    public function albumSharing(Request $request,$albumId,$token){

        /*To share a link here it will check album reference token and album id ,both of this id is encrypted which makes it secure*/
        /*It will verify given share url which is valid or not, if it is valid then user can get login page ,other wise it will refuse to next step*/
        $id =  Crypt::decryptString($albumId);
        $referenceToken = $token;


        $album = Album::where('id','=',$id)->first();

        if($album->token == $referenceToken ){
            return view('userLogin',['id'=>$albumId,'token'=>$referenceToken]);
        }else{
            return "Not Found";
        }
    }

    public function userAccess(Request $request){

        /*Here after getting valid url user will access photo via password, it check album password and given user password after matching user could get album photos */
        $password = ($request->input('password'));
        $token = ($request->input('token'));
        $id = Crypt::decryptString($request->input('album'));


        $album = Album::where('id','=',$id)->first();

        $comment = Comments::where('album_id','=',$id)->get();

        //return $comment;

        if (Hash::check($password,$album->password)) {

            $photo = Photo::where('album_id','=',$id)->get();

            return view('userViewAlbum',['photos'=> $photo,'id'=>$id,'comments' =>$comment]);
            //return 'Password Matched Successfully';
        }
        else{
            Session::flash('message', 'Please check your credentials or contact your administrator!');
            return redirect('album/sharing/'.$request->input('album').'/'.$token);
        }
    }

    public function saveUserComment(Request $request){

        /*It will save user comments*/

        $comment = new Comments();

        $comment->comment = $request->comment;
        $comment->album_id = $request->id;
        $comment->save();

        return response()->json($comment);
    }

    public function albumDelete(Request $request){

        /*It will delete user album along with photos*/

        $id = $request->id;

        $album = Album::destroy($id);

        $photo = Photo::where('album_id','=',$id)->get();

        if($photo){
            for($i=0;$i<count($photo);$i++){
                unlink(public_path('/images/photos/'. $photo[$i]->name));

            }
        }
        return response::json($album);
    }
}
