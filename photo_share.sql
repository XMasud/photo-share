-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 04, 2018 at 12:09 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `photo_share`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `name`, `user_id`, `token`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Masud', 1, '5CmXbUDZKMTmuN7aSIFtfQ1iYX2uo4', '$2y$10$0dIoRunXcfIzs7cE9iM5DeMdo4dTo/wFZj4bstFomy/cTO/4dEaHG', '2018-04-23 12:00:00', '2018-04-01 19:01:38'),
(2, 'New York City', 1, 'ztputXIzmxSN4tW2DsHbWsJKs1vrQ6', '$2y$10$/UFnuvZrDEJMJ7GUfX3NL.Fm8RYzLySqv/q3hOi19P/G4/BXro4PK', '2018-04-01 05:13:21', '2018-04-01 20:20:10'),
(40, 'Saint Martin, Trip - 2018', 1, 'jpVQWIRmdYqsfe6c42d5YE41GPNoIE', NULL, '2018-04-01 18:09:12', '2018-04-01 18:09:25'),
(42, 'Brac', 1, 'czq11mI3uKsUZYyqECHBqtOdTcCFvX', '$2y$10$gCvISF9/.KZFP.axdBqScu8ENNUqSSZuordNUiUNGJuHuGT9j58Xa', '2018-04-02 19:34:48', '2018-04-02 19:35:28'),
(43, 'ECE', 1, '3tpuuWhXHUI80KxRUmSMOODicdIrNa', NULL, '2018-04-03 22:30:30', '2018-04-03 22:30:30');

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `comment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `album_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `comment`, `album_id`, `created_at`, `updated_at`) VALUES
(1, 'Good Collection', 1, '2018-04-03 00:09:35', '2018-04-03 00:09:35'),
(2, 'Excellent collection..', 1, '2018-04-03 00:12:13', '2018-04-03 00:12:13'),
(3, 'oyee', 1, '2018-04-03 00:13:46', '2018-04-03 00:13:46'),
(4, 'Valo chobi', 1, '2018-04-03 00:19:59', '2018-04-03 00:19:59'),
(5, 'juetyte', 42, '2018-04-03 01:35:55', '2018-04-03 01:35:55');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(4, '2018_04_01_131820_create_photos_table', 3),
(5, '2018_03_30_180407_create_albums_table', 4),
(6, '2018_04_02_171553_create_comments_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `album_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `name`, `album_id`, `created_at`, `updated_at`) VALUES
(18, '152259699491798.jpg', 1, '2018-04-01 09:36:34', '2018-04-01 09:36:34'),
(19, '152259699486528.jpg', 1, '2018-04-01 09:36:34', '2018-04-01 09:36:34'),
(20, '152259699416866.jpg', 1, '2018-04-01 09:36:34', '2018-04-01 09:36:34'),
(21, '152259699421725.jpg', 1, '2018-04-01 09:36:34', '2018-04-01 09:36:34'),
(22, '152259699499917.jpg', 1, '2018-04-01 09:36:34', '2018-04-01 09:36:34'),
(23, '152259699499612.jpg', 1, '2018-04-01 09:36:34', '2018-04-01 09:36:34'),
(24, '152259699498725.jpg', 1, '2018-04-01 09:36:34', '2018-04-01 09:36:34'),
(25, '152265719487453.jpg', 2, '2018-04-02 02:19:54', '2018-04-02 02:19:54'),
(26, '152265719460100.jpg', 2, '2018-04-02 02:19:54', '2018-04-02 02:19:54'),
(27, '152268966211941.jpg', 3, '2018-04-02 11:21:02', '2018-04-02 11:21:02'),
(28, '152268966260097.jpg', 3, '2018-04-02 11:21:02', '2018-04-02 11:21:02'),
(29, '152274090553481.jpg', 42, '2018-04-03 01:35:05', '2018-04-03 01:35:05');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `api_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `api_token`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Masud Rana', 'masud@sfs.com', '$2y$10$ZQU2PtzzrDRlfLn7xXCsR.Q64RcTpfLHot8i3OyY7PdkO0q1g2nxK', 'Publisher', '7yGcBYLYFP8QM4FnG9eewHORRXmN8I322dyP3Upu3puAlCfqbNjtvKcgQFPC', 'EmTQM9xZ3WFhwpd5aIzRAsuRa1TC3cYjAT7Qt2MlU1ueL3LRE3VK8oiAQdn1', '2018-03-30 08:35:56', '2018-03-30 08:35:56'),
(2, 'Masud Rana', 'wq', '$2y$10$.zvRc5MssHPms8l3eJF0HeecY2Gr3QaHvY4SAjxsPbqdUsPRMroMi', 'Publisher', 'VsmpaFlh3jFHEpCrVdDuqLYqdXyk5h3KcOqkz028Lgx89YqmxMBWZzwHzfCc', NULL, '2018-03-30 08:39:08', '2018-03-30 08:39:08'),
(3, 'tgf', 'gfdg', '$2y$10$fOB5ctAItn7zdLw9OBDag.tgIyGuTpj6Ci9DptFK9vIwitdp7wzLm', 'Publisher', '2HDqK9qt1k6Clq3CG2BjXEZ5JCd2FnG6jXyofjrcB59yXqLJ0n9jlg5CyCVG', NULL, '2018-03-30 08:41:04', '2018-03-30 08:41:04'),
(4, 'rana', 'rana@sfs.com', '$2y$10$37WdNINhBZVuVS1n7CSx0O9NOW8jKsNlzgGPBQtQKie4U4yRB6LkG', 'Publisher', 'b75hInUvUAIBDKezsjLYKf9In4IAwq4tET89LEjIueApYEmWMCiCuEetVa6J', '5hl8pkB4GyWYqNXpoqaBH4FCfNFanj6xM2a6ur0XtDHcXWApo6CL3IRh4wNk', '2018-04-01 08:42:45', '2018-04-01 08:42:45');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
