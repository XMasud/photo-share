<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Album Share</title>

   {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>


    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <!-- Custom styles for this template -->
    <link href="{{ asset('/assets/css/3-col-portfolio.css') }}" rel="stylesheet"
          type="text/css"/>
    {{--<link href="css/3-col-portfolio.css" rel="stylesheet">--}}

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Photo Album Share</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/publisher/home')}}">Home

                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="{{url('add/album')}}">Album</a>
                    <span class="sr-only">(current)</span>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url('/logout')}}">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <div>
        <h3 class="my-4">
            <small><a href="#">Add Album</a> </small>
            <button type="button" style="float: right;margin-bottom: 15px;"  class="btn btn-primary add-album">Add Album</button>
        </h3>
    </div>




    <div class="container">


        <table class="table table-bordered table-hover albumTable">
            <thead>
            <tr>
                <th class="text-center">#</th>
                <th class="text-center">Name</th>
                <th class="text-center">Created At</th>
                <th class="text-center">Action</th>
            </tr>
            </thead>
            <tbody>

            @if(!$albums->isEmpty())
                @foreach($albums as $album)
                    <tr class="item{{$album->id}}">
                        <td class="text-center">{{$album->id}}</td>
                        <td class="text-center">{{$album->name}}</td>
                        <td class="text-center">{{$album->created_at}}</td>
                        <td class="text-center">

                            <button class="edit-modal btn btn-info" data-id="{{$album->id}}" data-name="{{$album->name}}"><span class="glyphicon glyphicon-edit"></span> Edit</button>
                            <button class="delete-modal btn btn-danger" data-id="{{$album->id}}" data-name="{{$album->name}}"><span class="glyphicon glyphicon-trash"></span> Delete</button>
                            <button class="go-album btn btn-info" data-id="{{$album->id}}" data-name="{{$album->name}}"><span class="glyphicon glyphicon-trash"></span> Go</button>
                        </td>
                    </tr>
                @endforeach
            @else
                <div class="alert alert-warning col-sm-12 col-md-12">
                    <strong>Warning!</strong> There is no album in your profile .
                </div>
            @endif


            </tbody>
        </table>



        <nav aria-label="Page navigation example">
            <ul class="pagination">
               {{-- {{ $albums->links() }}--}}

            </ul>
        </nav>


    </div>
    <!-- /.row -->

    <!-- Pagination -->

    <div class="modal fade" id="albumModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Album</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="albumForm" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label " for="email">Album Name :</label>
                            <div class="form-group">
                                <input type="text" class="form-control" id="album" placeholder="Enter Album Name" name="album">
                                <span style="color: red;display: none" id="addAlbumError">Album Name Required...</span>
                            </div>
                        </div>
                    </div>
                    {{ csrf_field() }}
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-token="{{ csrf_token() }}" id="saveAlbum">Save Album</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


    <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit Album</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="albumForm" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label " for="email">Album Name :</label>
                            <div class="form-group">
                                <input type="text" class="form-control" id="album_name"  name="album_name">
                                <span style="color: red;display: none" id="editAlbumError">Album Name Required...</span>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" id="album_id">
                    {{ csrf_field() }}
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-token="{{ csrf_token() }}" id="save-edit">Save Change</button>
                    </div>
                </form>

            </div>
        </div>
    </div>


    <div class="modal fade" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">

                    <h4 class="modal-title">Delete Parmanently</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p style="color: red;">Are you sure about this ? If there is any photo contain in this album,it will be deleted</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    <button type="button" class="btn btn-danger" id="confirm">Delete</button>
                </div>
            </div>
        </div>
    </div>



</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Photo Share 2018</p>
    </div>
    <!-- /.container -->
</footer>

{{--<footer class="footer bg-dark">
    <div class="container">
        <span class="text-muted">Place sticky footer content here.</span>
    </div>
</footer>--}}

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('/assets/vendor/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>

<script>

    $('#saveAlbum').click(function () {

        var location = window.location.origin;

        var name = $('#album').val();

        //alert(name);

        if(name){
            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: location+'/save/album',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'name': $('#album').val()

                },
                success: function(data) {

                    console.log(data);

                    if((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else{
                        $('.error').addClass('hidden');
                        //$('.albumTable').append("<tr class='item" + data.id + "'><td class='text-center'>" + data.id + "</td><td class='text-center'>" + data.name +"</td><td class='text-center'>" + data.created_at+ "</td><td class='text-center'><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "' data-created_at='" + data.created_at + "'> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "' data-created_at='" + data.created_at + "'> Delete</button></td></tr>")
                        $('.albumTable').append("<tr class='item" + data.id + "'><td class='text-center'>" + data.id + "</td><td class='text-center'>" + data.name + "</td><td class='text-center'>" + data.created_at + "</td><td class='text-center'><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-trash'></span> Delete</button> <button class='go-album btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-trash'></span> Go</button></td></tr>");

                        $('#album').val('');
                        $('#albumModal').modal('hide');

                    }
                },
            });
        }else{
            $('#addAlbumError').css('display', "block");
        }

    });

;

    $(document).on('click', '.edit-modal', function() {
        $('#album_id').val($(this).data('id'));
        $('#album_name').val($(this).data('name'));
        $('#editModal').modal('show');

        $('#save-edit').click(function () {

            var name = $('#album_name').val();
            if(name){
                var location =  window.location.origin;

                $.ajax({
                    type: 'get',
                    url: location+'/edit/album',
                    data: {
                        '_token': $('input[name=_token]').val(),
                        'id': $("#album_id").val(),
                        'name': $('#album_name').val()
                    },
                    success: function(data) {

                        $('.item' + data.id).replaceWith("<tr class='item" + data.id + "'><td class='text-center'>" + data.id + "</td><td class='text-center'>" + data.name + "</td><td class='text-center'>" + data.created_at + "</td><td class='text-center'><button class='edit-modal btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-edit'></span> Edit</button> <button class='delete-modal btn btn-danger' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-trash'></span> Delete</button> <button class='go-album btn btn-info' data-id='" + data.id + "' data-name='" + data.name + "'><span class='glyphicon glyphicon-trash'></span> Go</button></td></tr>");

                        $('#editModal').modal('hide');
                        $('#album_id').val($(this).data(''));
                        $('#album_name').val($(this).data(''));

                    }
                });

            }else{
                $('#editAlbumError').css('display', "block");
            }
        })
    });

    $(document).on('click', '.delete-modal', function() {

        $('#confirmDelete').modal('show');
        var id = ($(this).data("id"));

        $('#confirm').click(function () {


            var location = window.location.origin;


            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: location+'/delete/album',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': id

                },
                success: function(data) {

                    console.log(data);

                    if((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else{
                        $('.error').addClass('hidden');
                        console.log(id);
                        $(".item" + id).remove();
                        $('#confirmDelete').modal('hide');

                    }
                },
            });


        })
    })

    $(document).on('click', '.go-album', function() {
        var id = $(this).data('id');
        location.href =  '/album/'+id;
       // alert(id);
    });


    $('.add-album').click(function () {
        $('#albumModal').modal('show');
    })
</script>

</body>

</html>
