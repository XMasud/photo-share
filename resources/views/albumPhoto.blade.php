<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Album Share</title>

    {{-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
    {{--Image--}}



    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <!-- Custom styles for this template -->
    <link href="{{ asset('/assets/css/3-col-portfolio.css') }}" rel="stylesheet"
          type="text/css"/>
    {{--<link href="css/3-col-portfolio.css" rel="stylesheet">--}}


    <style>

        .entry:not(:first-of-type)
        {
            margin-top: 10px;
        }

        .glyphicon
        {
            font-size: 12px;
        }
    </style>


</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Photo Album Share</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/publisher/home')}}">Home

                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('add/album')}}">Album</a>
                    <span class="sr-only">(current)</span>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url('/logout')}}">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <div>
        <h3 class="my-4">
            <small>Add Photo </small>
        </h3>
    </div>

    <div class="container">

        <div class="row">
            <div class="col-4">

                <div class="list-group">
                    <a href="#" class="list-group-item active">Album Information</a>
                    <a href="#" class="list-group-item">Album Name: {{$album->name}} </a>
                    <a href="#" class="list-group-item">Publisher Name: {{$album->user->name}}</a>
                    <a href="#" class="list-group-item">Created At: {{$album->created_at}}</a>

                </div>
            </div>
            <div class="col-6">

                <form id="photoForm" action="{{url('photo/album')}}" files="true" role="form"  method="POST" enctype="multipart/form-data">

                    {{ csrf_field() }}

                    <div class="control-group" id="fields">
                        <label class="control-label" for="field1">
                            Upload Your Photos
                        </label>
                        <div class="controls">

                            <div class="entry input-group col-xs-3">

                                <input class="btn btn-primary" id="images" name="images[]" type="file">
                                <span class="input-group-btn"><button class="btn btn-success btn-add" type="button"><span class="glyphicon glyphicon-plus"></span>Add More</button></span>
                            </div>
                            <span id="imageError" style="color: red;display: none;">Image field can not be null...</span>
                        </div>
                        <br>
                        <input type="hidden" name="album_id" value="{{$album->id}}">
                        <div class="controls">
                            <button class="btn btn-info btn-submit" type="button"><span class="glyphicon glyphicon-plus"></span>Upload Photos</button></span>
                        </div>

                    </div>
                </form>

                <a href="{{url('view/album/'.Crypt::encryptString($album->id))}}">Go Photos</a>

            </div>

        </div>







    </div>
    <!-- /.row -->

    <!-- Pagination -->






</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Photo Share 2018</p>
    </div>
    <!-- /.container -->
</footer>

{{--<footer class="footer bg-dark">
    <div class="container">
        <span class="text-muted">Place sticky footer content here.</span>
    </div>
</footer>--}}

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('/assets/vendor/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>

<script>

    $(function()
    {
        $(document).on('click', '.btn-add', function(e)
        {
            e.preventDefault();

            var controlForm = $('.controls:first'),
                currentEntry = $(this).parents('.entry:first'),
                newEntry = $(currentEntry.clone()).appendTo(controlForm);

            newEntry.find('input').val('');
            controlForm.find('.entry:not(:last) .btn-add')
                .removeClass('btn-add').addClass('btn-remove')
                .removeClass('btn-success').addClass('btn-danger')
                .html('<span class="glyphicon glyphicon-minus">Remove</span>');
        }).on('click', '.btn-remove', function(e)
        {
            $(this).parents('.entry:first').remove();

            e.preventDefault();
            return false;
        });
    });

    $('.btn-submit').click(function () {

        var image = $('#images').val();

           if(image){
               $('#photoForm').submit();
           }else{
               $("#imageError").css("display", "block");
           }

    })


</script>

</body>

</html>
