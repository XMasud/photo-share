<link href="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.1.0/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<div class="container">


    <div id="loginbox" style="margin-top:50px;" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info" >
            <div class="panel-heading">
                <div class="panel-title">Sign In</div>

            </div>

            <div style="padding-top:30px" class="panel-body" >

                <div style="display:none" id="login-alert" class="alert alert-danger col-sm-12"></div>

                <form id="loginform" method="post" action="{{url('/verify')}}" class="form-horizontal" role="form">

                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
                        <input id="login-username" type="email" class="form-control" name="email" value="" placeholder="username or email">
                    </div>
                    {{ csrf_field() }}
                    <div style="margin-bottom: 25px" class="input-group">
                        <span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
                        <input id="login-password" type="password" class="form-control" name="password" placeholder="password">
                    </div>

                    @if(Session::has('message'))
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            <strong>@php echo Session::get("message"); @endphp</strong>
                        </div>

                    @endif

                    <div class="input-group">
                        <div class="checkbox">
                            <label>
                                <input id="login-remember" type="checkbox" name="remember" value="1"> Remember me
                            </label>
                        </div>
                    </div>


                    <div style="margin-top:10px" class="form-group">

                            <!-- Button -->
                            <div class="col-md-offset-3 col-md-9">
                                <button id="btnSubmit" type="submit" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Login</button>

                            </div>



                    </div>


                    <div class="form-group">
                        <div class="col-md-12 control">
                            <div style="border-top: 1px solid#888; padding-top:15px; font-size:85%" >
                                Don't have an account!
                                <a href="#" onClick="$('#loginbox').hide(); $('#signupbox').show()">
                                    Sign Up Here
                                </a>
                            </div>
                        </div>
                    </div>
                </form>



            </div>
        </div>
    </div>
    <div id="signupbox" style="display:none; margin-top:50px" class="mainbox col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Sign Up</div>
                <div style="float:right; font-size: 85%; position: relative; top:-10px"><a id="signinlink" href="#" onclick="$('#signupbox').hide(); $('#loginbox').show()">Sign In</a></div>
            </div>
            <div class="panel-body" >
                <form id="signup-form" action="{{url('/publisher')}}" method="post" class="form-horizontal" role="form">

                    <div id="signupalert"  style="display:none" class="alert alert-danger">
                        <p>Error:</p>
                        <span></span>
                    </div>

                    {{ csrf_field() }}

                    <div class="form-group">
                        <label for="firstname" class="col-md-3 control-label">Name</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control required" name="name" placeholder="Enter Name">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-md-3 control-label">Email</label>
                        <div class="col-md-9">
                            <input type="text" class="form-control required" name="email" placeholder="Email Address">
                        </div>
                    </div>


                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control required" name="password" id="password" placeholder="Password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-md-3 control-label">Confirm Password</label>
                        <div class="col-md-9">
                            <input type="password" class="form-control required" name="password2" id="password2" onchange="myFunction();" placeholder="Confirm Password">
                            <label style="color: red;display: none;" id="password_match"> Password does not match...</label>
                        </div>

                    </div>



                    <div class="form-group">
                        <!-- Button -->
                        <div class="col-md-offset-3 col-md-9">
                            <button id="btn-signup" type="button" class="btn btn-info"><i class="icon-hand-right"></i> &nbsp Sign Up</button>

                        </div>
                    </div>





                </form>
            </div>
        </div>




    </div>
</div>

<script>

    /*function myFunction() {

        var password = $('#password').val();
        var confirmPassword = $('#password2').val();

        if(password != confirmPassword){

            alert(2);
           // $("#password_match").show();
            $('#btn-signup').prop('disabled', true);
        }else{

            $("#password_match").css("display", "none");
            $('#btn-signup').prop('disabled', false);

        }
    }*/

    $('#btn-signup').click(function () {
        var flag = 1;
        var isValid = true;
        var passwordConfirm = 1;
        $('.required').each(function () {
            if ($.trim($(this).val()) == '') {
                isValid = false; /* Required class style */
                $(this).css({
                    "border": "1px solid red",
                    "background": "#FFCECE"
                });
            }
            else { /* Required class style removed */
                $(this).css({
                    "border": "",
                    "background": ""
                });
            }
        });
        if (isValid == false){
            flag = 2;
        }
        else{
            flag = 1;
        }
        var password = $('#password').val();
        var confirmPassword = $('#password2').val();

        if(password != confirmPassword){

            passwordConfirm = 2;
            $("#password_match").show();

        }else{
            passwordConfirm = 1;
            $("#password_match").hide();

        }

        if(flag ==1 && passwordConfirm == 1){
           // alert('Okkay');
            $('#signup-form').submit();
        }else{
            //alert('Not Okkay');
        }


    });

    $('.required').click(function () {
        $(this).css({
            "border": "",
            "background": ""
        });
    });

</script>