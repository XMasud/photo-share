<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Album Share</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <!-- Custom styles for this template -->
    <link href="{{ asset('/assets/css/3-col-portfolio.css') }}" rel="stylesheet"
          type="text/css"/>
    {{--<link href="css/3-col-portfolio.css" rel="stylesheet">--}}

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Photo Album Share</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{url('/publisher/home')}}">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('add/album')}}">Album</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url('/logout')}}">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <h3 class="my-4">Albums
        <small><a href="{{url('add/album')}}">Add Album</a> </small>
    </h3>

    <div class="row">

        @if(!$albums->isEmpty())
            @foreach($albums as $album)
                <div class="col-lg-4 col-sm-6 portfolio-item">
                    <div class="card h-100">
                        <a href="{{url('view/album/'.Crypt::encryptString($album->id))}}"><img class="card-img-top" src="http://placehold.it/700x400" alt=""></a>
                        <div class="card-body">
                            <h5 class="card-title">
                                <a href="{{url('view/album/'.Crypt::encryptString($album->id))}}">{{$album->name}}</a>
                                <button style="float: right;" type="button" class="btn btn-primary btn-sm password-btn" data-id="{{$album->id}}">Set Password</button>
                                <button style="float: right;margin-right: 2px;" type="button" class="btn btn-success btn-sm share-btn"  data-id="{{Crypt::encryptString($album->id)}}" data-token="{{$album->token}}" >Share</button>

                            </h5>

                        </div>
                    </div>
                </div>
            @endforeach

        @else
            <div class="alert alert-warning col-md-12">
                <strong>Warning!</strong> There is no album in your profile .
            </div>
        @endif





    </div>
    <!-- /.row -->

    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Share Album</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">

                    <div class="form-control">
                        <input id="link" type="text" class="form-control" readonly>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save changes</button>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="passwordModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Set password for album</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="albumForm" method="post" enctype="multipart/form-data">
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label " for="email">Password :</label>
                            <div class="form-group">
                                <input type="password" class="form-control" id="password"  name="password">
                                <span id="passwordError" style="color: red;display: none;">Password Required ...</span>
                            </div>

                        </div>
                    </div>
                    <input type="hidden" id="album_id">
                    {{ csrf_field() }}
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" data-token="{{ csrf_token() }}" id="save-password">Save Change</button>
                    </div>
                </form>

            </div>
        </div>
    </div>




    <!-- Pagination -->
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
    </ul>

</div>
<!-- /.container -->




<!-- Footer -->
<footer class="py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Photo Share 2018</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('/assets/vendor/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
</body>

<script>

    $(document).on('click', '.password-btn', function() {

        $('#album_id').val($(this).data('id'));
        $("#passwordError").css("display", "none");
        $('#passwordModal').modal('show');

    });

    $('#save-password').click(function () {


        var location = window.location.origin;

        if($('#password').val()) {
            $.ajax({
                type: 'post',
                url: location + '/album/password',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $("#album_id").val(),
                    'password': $('#password').val()
                },
                success: function (data) {

                    console.log(data);
                    $('#passwordModal').modal('hide');
                    $("#album_id").val('');
                    $('#password').val('');
                }
            })
        }else{
            $("#passwordError").css("display", "block");
        }
    });

    $('.share-btn').click(function () {

        var location = window.location.origin;


        $('#exampleModal').modal('show');
        $('#link').val(location+'/album/sharing/'+$(this).data('id')+'/'+$(this).data('token'));

    })


</script>

</html>
