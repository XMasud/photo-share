<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Album Share</title>

    <!-- Bootstrap core CSS -->
    <link href="{{ asset('/assets/vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet"
          type="text/css"/>

    <!-- Custom styles for this template -->
    <link href="{{ asset('/assets/css/3-col-portfolio.css') }}" rel="stylesheet"
          type="text/css"/>
    {{--<link href="css/3-col-portfolio.css" rel="stylesheet">--}}
    <link href="https://fonts.googleapis.com/css?family=Droid+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.css">

    <link href="{{ asset('/assets/css/gallery-grid.css') }}" rel="stylesheet"
          type="text/css"/>

    {{--Comment Section--}}


    {{--  <script src='https://production-assets.codepen.io/assets/editor/live/events_runner-73716630c22bbc8cff4bd0f07b135f00a0bdc5d14629260c3ec49e5606f98fdd.js'></script>

      <script src='https//production-assets.codepen.io/assets/editor/live/css_live_reload_init-2c0dc5167d60a5af3ee189d570b1835129687ea2a61bee3513dee3a50c115a77.js'></script>--}}
    <meta charset='UTF-8'><meta name="robots" content="noindex"><link rel="shortcut icon" type="image/x-icon" href="https://production-assets.codepen.io/assets/favicon/favicon-8ea04875e70c4b0bb41da869e81236e54394d63638a1ef12fa558a4a835f1164.ico" />
    <link rel="mask-icon" type="" href="//production-assets.codepen.io/assets/favicon/logo-pin-f2d2b6d2c61838f7e76325261b7195c27224080bc099486ddd6dccb469b8e8e6.svg" color="#111" />
    <link rel="canonical" href="https://codepen.io/kavendish/pen/aOdopx?q=comment&limit=all&type=type-pens" />
    <link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <meta name="viewport" content="width=device-width">

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>

    <style class="cp-pen-styles">html, body {
            background-color: #f0f2fa;
            font-family: "PT Sans", "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
            color: #555f77;
            -webkit-font-smoothing: antialiased;
        }

        input, textarea {
            outline: none;
            border: none;
            display: block;
            margin: 0;
            padding: 0;
            -webkit-font-smoothing: antialiased;
            font-family: "PT Sans", "Helvetica Neue", "Helvetica", "Roboto", "Arial", sans-serif;
            font-size: 1rem;
            color: #555f77;
        }
        input::-webkit-input-placeholder, textarea::-webkit-input-placeholder {
            color: #ced2db;
        }
        input::-moz-placeholder, textarea::-moz-placeholder {
            color: #ced2db;
        }
        input:-moz-placeholder, textarea:-moz-placeholder {
            color: #ced2db;
        }
        input:-ms-input-placeholder, textarea:-ms-input-placeholder {
            color: #ced2db;
        }

        p {
            line-height: 1.3125rem;
        }

        .comments {
            margin: 2.5rem auto 0;
            max-width: 60.75rem;
            padding: 0 1.25rem;
        }

        .comment-wrap {
            margin-bottom: 1.25rem;
            display: table;
            width: 100%;
            min-height: 5.3125rem;
        }

        .photo {
            padding-top: 0.625rem;
            display: table-cell;
            width: 3.5rem;
        }
        .photo .avatar {
            height: 2.25rem;
            width: 2.25rem;
            border-radius: 50%;
            background-size: contain;
        }

        .comment-block {
            padding: 1rem;
            background-color: #fff;
            display: table-cell;
            vertical-align: top;
            border-radius: 0.1875rem;
            box-shadow: 0 1px 3px 0 rgba(0, 0, 0, 0.08);
        }
        .comment-block textarea {
            width: 100%;
            resize: none;
        }

        .comment-text {
            margin-bottom: 1.25rem;
        }

        .bottom-comment {
            color: #acb4c2;
            font-size: 0.875rem;
        }

        .comment-date {
            float: left;
        }

        .comment-actions {
            float: right;
        }
        .comment-actions li {
            display: inline;
            margin: -2px;
            cursor: pointer;
        }
        .comment-actions li.complain {
            padding-right: 0.75rem;
            border-right: 1px solid #e1e5eb;
        }
        .comment-actions li.reply {
            padding-left: 0.75rem;
            padding-right: 0.125rem;
        }
        .comment-actions li:hover {
            color: #0095ff;
        }
    </style>

</head>

<body>

<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container">
        <a class="navbar-brand" href="#">Photo Album Share</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item ">
                    <a class="nav-link" href="{{url('/publisher/home')}}">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{url('add/album')}}">Album</a>
                </li>

                <li class="nav-item">
                    <a class="nav-link" href="{{url('/logout')}}">Logout</a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<!-- Page Content -->
<div class="container">

    <!-- Page Heading -->
    <h3 class="my-4">
        <small>View Album Photos </small>
    </h3>

    <div class="row">

        <div class="tz-gallery">

            <div class="row">
                @if(!$photos->isEmpty())
                    @foreach($photos as $photo)
                        <div class="col-sm-6 col-md-4">
                            <a class="lightbox" href="{{ asset('/images/photos/'.$photo->name) }}">
                                <img src="{{ asset('/images/photos/'.$photo->name) }}" alt="Park">
                            </a>
                        </div>
                    @endforeach
                @else
                    <div class="alert alert-warning col-sm-12 col-md-12">
                        <strong>Warning!</strong> There is no photos in your album .
                    </div>
                @endif

            </div>

        </div>





        <input type="hidden" id="album_id" value="{{$id}}">
    </div>
    <!-- /.row -->

    <!-- Pagination -->
    <ul class="pagination justify-content-center">
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
                <span class="sr-only">Previous</span>
            </a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">1</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">2</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#">3</a>
        </li>
        <li class="page-item">
            <a class="page-link" href="#" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
                <span class="sr-only">Next</span>
            </a>
        </li>
    </ul>


    <div class="comments">

        <form >
            <div class="comment-wrap">
                <div class="photo">
                    <div class="avatar" style="background-image: url('https://s3.amazonaws.com/uifaces/faces/twitter/dancounsell/128.jpg')"></div>
                </div>
                <div class="comment-block">
                    <form action="">
                        <textarea name="" id="comment" cols="30" rows="2" placeholder="Add comment..."></textarea>
                    </form>
                </div>
            </div>
            <button type="button" style="float: left;margin-top: -17px;margin-left: 56px;" class="btn btn-info comment-btn">Post</button>
        </form>



        @foreach($comments as $comment)
            <div class="comment-wrap">
                <div class="photo">
                    <div class="avatar" style="background-image: url('https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg')"></div>
                </div>
                <div class="comment-block">
                    <p class="comment-text">{{$comment->comment}}</p>
                    <div class="bottom-comment">
                        <div class="comment-date">{{$comment->created_at->diffForHumans()}}</div>
                        <ul class="comment-actions">
                            <li class="complain">Delete</li>
                            <li class="reply">Reply</li>
                        </ul>
                    </div>
                </div>
            </div>
        @endforeach


    </div>

</div>
<!-- /.container -->

<!-- Footer -->
<footer class="py-3 bg-dark">
    <div class="container">
        <p class="m-0 text-center text-white">Copyright &copy; Photo Share 2018</p>
    </div>
    <!-- /.container -->
</footer>

<!-- Bootstrap core JavaScript -->
<script src="{{ asset('/assets/vendor/jquery/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>

<script>
    $(".comment-btn").click(function () {

        //alert(10);
        var comment = $('#comment').val();

        if(comment) {

            var location = window.location.origin;

            $.ajaxSetup ({
                // Disable caching of AJAX responses
                cache: false
            });
            $.ajax({
                type: 'get',
                url: location+'/album/comment',
                data: {
                    '_token': $('input[name=_token]').val(),
                    'id': $('#album_id').val(),
                    'comment': $('#comment').val()

                },
                success: function(data) {

                    console.log(data);

                    if((data.errors)){
                        $('.error').removeClass('hidden');
                        $('.error').text(data.errors.name);
                    }
                    else{
                        $('.error').addClass('hidden');

                        $('.comments').append("<div class=\"comment-wrap\">\n" +
                            "            <div class=\"photo\">\n" +
                            "                <div class=\"avatar\" style=\"background-image: url('https://s3.amazonaws.com/uifaces/faces/twitter/jsa/128.jpg')\"></div>\n" +
                            "            </div>\n" +
                            "            <div class=\"comment-block\">\n" +
                            "                <p class=\"comment-text\">\n" +data.comment+
                            "                    </p>\n" +
                            "                <div class=\"bottom-comment\">\n" +
                            "                    <div class=\"comment-date\">"+data.created_at+"</div>\n" +
                            "                    <ul class=\"comment-actions\">\n" +
                            "                        <li class=\"complain\">Delete</li>\n" +
                            "                        <li class=\"reply\">Reply</li>\n" +
                            "                    </ul>\n" +
                            "                </div>\n" +
                            "            </div>\n" +
                            "        </div>");

                        $('#comment').val($(this).data(''));


                    }
                },
            });




        }
    });
</script>
</body>

</html>
