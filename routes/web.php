<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Default Index page of this project*/
Route::get('/', function () {
    return view('mainView');
});

/*Login and Registration*/
Route::get('/login', function () {
    return view('auth.login');
});
Auth::routes();


/*Login verify*/
Route::post('verify', 'CustomController@loginCheck');

/*Add Publisher in this system,who can login*/
Route::post('/publisher', 'PublisherController@addPublisher');

/*Get link to access photo album*/
Route::get('album/sharing/{albumId}/{token}', 'PublisherController@albumSharing');

/*User password verify to view album photo for given link*/
Route::post('user/view', 'PublisherController@userAccess');

/*User comment on photo album*/
Route::get('album/comment', 'PublisherController@saveUserComment');

/*Setting middleware to restrict privilege for any one */
Route::group(['middleware' => 'UserAuth'], function(){

    /*After successful login,view album home */
    Route::get('publisher/home', 'PublisherController@viewHome');

    Route::get('/home', 'HomeController@index')->name('home');
    /*Publisher logout*/
    Route::get('/logout', 'CustomController@logout');

    /*Adding album and view album list page*/
    Route::get('add/album', 'PublisherController@albumView');

    /*Save new album*/
    Route::get('save/album', 'PublisherController@saveAlbum');
    /*Edit album*/
    Route::get('edit/album', 'PublisherController@editAlbum');
    /*Go to individual album and can upload and view album photo*/
    Route::get('album/{id}', 'PublisherController@goAlbum');

    /*Save album photo*/
    Route::post('photo/album', 'PublisherController@addPhoto');

    /*View album*/
    Route::get('view/album/{id}', 'PublisherController@viewPhoto');

    /*Set password for album to share with others*/
    Route::post('album/password', 'PublisherController@albumPasswordSet');

    /*Delete album but delete album also delete album photos*/
    Route::get('delete/album', 'PublisherController@albumDelete');


});